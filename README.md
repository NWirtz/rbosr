# <img src="doc/pictures/rbosr.png" width=80 /> Rule-Based Optimized Service Restoration (RBOSR)

## Introduction

The service restoration aims at reconfiguring the electrical network after the occurrence of faults, in which the protection system coordinates the tripping of circuit breakers upstream and downstream of the fault location.
Distribution networks are managed with radial scheme; therefore, the nodes downstream of the faulted zone become de-energized and they have to be re-powered from an alternative source.
The algorithm present in this repository considers, among the de-energized nodes, the one with highest priority as target for the restoration and identifies which is the most suitable primary substation to energize it, by closing the normally open bus-tie unit.
The restoration schemes from each substation toward the target node are evaluated with a state estimation approach (which also allows to check the voltage, thermal and radiality constraints); the algorithm considers total power losses and utilization of the most consumed electrical lines as objectives included in the Multi-Critera Decision Making (MCDM) approach, set by the users.
Once the selected tie unit successfully closes, the process repeats until all the de-energized loads are restored or the constraints are violated. The algorithm is able to manages multiple/cascade faults in active distribution grids (with Distributed Energy Resources).

## Contents

This repository contains the following material:

- Python code [`RBO_Service_Restoration.py`](https://git.rwth-aachen.de/acs/public/automation/rbosr/-/blob/master/RBO_Service_Restoration.py.py) of rule-based optimization algorithm for service restoration (RBOSR).
- Python code [`DSSE_Functions_FLISR_1.py`](https://git.rwth-aachen.de/acs/public/automation/rbosr/-/blob/master/dsse_functions_FLISR_1.py) that includes the distribution system state estimator functions
- Excel file [`networkdata_FLIS.xlsx`](https://git.rwth-aachen.de/acs/public/automation/rbosr/-/blob/master/networkdata_FLISR.xlsx) that includes all the necessary grid data to run the service restoration process.
- PDF file [`Networkdata_MV_FLISR_use_case.pdf`](https://git.rwth-aachen.de/acs/public/automation/rbosr/blob/master/Networkdata_MV_FLISR_use_case.pdf) that summarizes the relevant grid data.

## Usage

Instructions to use the service restoration algorithm:

 1. The two Python codes and the excel file must be located in the same folder.
 2. Configure the excel sheet, providing information about grid topology, loads, switches and fault location (set the tripped circuit breakers).
 3. If changed, set the excel file name in the RBOSR Python code (in line 910).
 4. Set the comparison factors `uL1`, `uL2`, `uL3`, `u12`, `u13` and `u23` according to the MCDM desired restoration targets in the RBO_Service_Restoration Python code (in line 928). Additional information are found in the papers "How to make a decision: The analytic hierarchy process", author: Thomas L. Saaty and A. Dognini, A. Sadu, A. Angioni, F. Ponci and A. Monti, "Service Restoration Algorithm for Distribution Grids under High Impact Low Probability Events," 2020 IEEE PES Innovative Smart Grid Technologies Europe (ISGT-Europe), The Hague, Netherlands, 2020, pp. 237-241.
 5. Run the Python code `RBO_Service_Restoration.py`, which retrieves the faulted grid data, computes the possible existing solutions and publishes the results. 


Considerations about the RBOSR Python code:

 - The code has been developed with Python 3.7
 - It needs, among others, the installation of the following libraries:
   - `pandas`
   - `networkx`
   - `psycopg2`

 Run the following command to install the required dependencies:

```
pip install -r requirements.txt
```

## Documentation

Documentation about the distribution system state estimator can be found in the following website:
https://git.rwth-aachen.de/acs/public/automation/rbosr

## Publications

For additional information, refer to the following articles:

- A. Dognini, A. Sadu, A. Angioni, F. Ponci and A. Monti, [`Service Restoration Algorithm for Distribution Grids under High Impact Low Probability Events`](https://ieeexplore.ieee.org/document/9248823), 2020 IEEE PES Innovative Smart Grid Technologies Europe (ISGT-Europe), The Hague, Netherlands, 2020, pp. 237-241.

- C. Muscas, S. Sulis, A. Angioni, F. Ponci, and A. Monti, [`Impact of Different Uncertainty Sources on a Three-Phase State Estimator for Distribution Networks`](https://ieeexplore.ieee.org/document/6775299), IEEE Transactions on Instrumentation and Measurement, vol. 63, no. 9, pp. 2200-2209, Sep. 2014.
	
- A. Angioni, A. Kulmala, D. D. Giustina, M. Mirz, A. Mutanen, A. Ded,F.  Ponci,  L.  Shengye,  G.  Massa,  S.  Repo,  and  A.  Monti, [`Design  and implementation of a substation automation unit`](https://ieeexplore.ieee.org/document/7579641), IEEE Transactions on Power Delivery, vol. 32, no. 2, pp. 1133-1142, April 2017.
	
- P. Jamborsalamati, A. Sadu, F. Ponci, A. Monti and M. J. Hossain, [`Improvement of supply  restoration in multi-spare-feeder active distribution grids using IEC 61850`](https://ieeexplore.ieee.org/document/8378326), 2017 IEEE Innovative Smart Grid Technologies - Asia (ISGT-Asia), pp. 1-5, Dec 2017.

- Thonas L. Saaty "How to make a decision: The analytic hierarchy process", European Journal of Operational Research, Volume 48, Issue 1, 5 September 1990, Pages 9-26, https://doi.org/10.1016/0377-2217(90)90057-I

## Copyright

2019, Institute for Automation of Complex Power Systems, EONERC

## License

This project is released under the terms of the [GPL version 3](COPYING.md).

```
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```

For other licensing options please consult [Prof. Antonello Monti](mailto:amonti@eonerc.rwth-aachen.de).

## Contact

[![EONERC ACS Logo](doc/pictures/eonerc_logo.png)](http://www.acs.eonerc.rwth-aachen.de)

- Alberto Dognini <adognini@eonerc.rwth-aachen.de>
- Abhinav Sadu <asadu@eonerc.rwth-aachen.de>

[Institute for Automation of Complex Power Systems (ACS)](http://www.acs.eonerc.rwth-aachen.de)
[EON Energy Research Center (EONERC)](http://www.eonerc.rwth-aachen.de)
[RWTH University Aachen, Germany](http://www.rwth-aachen.de)
